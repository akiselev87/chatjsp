# Simple Chat Application #
This is a simple chat application to send messages between friends. There is no security, no modern-looking user interface.

This is just the very simple beginning with JSP (keyworkd here is Java) and database queries.

## Key features ##
* Register
* Login
* Send/Approve Friendship Request
* View Friends list
* Ignore Users
* Send Message
* Review Message
* Delete an Account

## Technology Used ##
* MySQL database
* JSP as a frontend
* JSP as a backend