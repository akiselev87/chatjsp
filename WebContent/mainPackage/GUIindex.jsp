<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Simple Chat</title>
</head>
<body>
	<form method="post" action="login.jsp">
		<table border="1">
			<thead>
				<tr>
					<th colspan="2">Login Here</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>E-mail</td>
					<td><input type="text" name="email" value="" width="100%" /></td>
				</tr>
				<tr>
					<td>Password</td>
					<td><input type="password" name="pwd" value="" width="100%" /></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit" value="Login" /></td>
				</tr>
				<tr>
					<td></td>
					<td colspan="2" align="left"><a href="GUIregistration.jsp">Register
							Here</a></td>
				</tr>
			</tbody>
		</table>
	</form>
</body>
</html>