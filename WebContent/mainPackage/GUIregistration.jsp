<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Registration</title>
</head>
<body>
	<form method="post" action="registration.jsp">
		<table border="1">
			<thead>
				<tr>
					<th colspan="2">Enter Information Here</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>First Name</td>
					<td><input type="text" name="fname" value="" maxlength="45" /></td>
				</tr>
				<tr>
					<td>Last Name</td>
					<td><input type="text" name="lname" value="" maxlength="45" /></td>
				</tr>
				<tr>
					<td>Email</td>
					<td><input type="text" name="email" value="" maxlength="45" /></td>
				</tr>
				<tr>
					<td>Password</td>
					<td><input type="password" name="pass" value="" maxlength="45" /></td>
				</tr>
				<tr>
					<td>Confirm Password</td>
					<td><input type="password" name="passConfirm" value=""
						maxlength="45" /></td>
				</tr>
				<tr>
					<td><input type="submit" value="Submit" /></td>
					<td><input type="reset" value="Reset" /></td>
				</tr>
				<tr>
					<td colspan="2">Already registered? <a href="GUIindex.jsp">Login
							Here</a></td>
				</tr>
			</tbody>
		</table>
	</form>
</body>
</html>