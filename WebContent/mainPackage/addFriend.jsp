<%@ page import="java.sql.*"%>
<%@ page import="java.util.regex.Matcher"%>
<%@ page import="java.util.regex.Pattern"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Simple Chat</title>
</head>
<%
//patterns
Pattern pattern;
Matcher matcher;
String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
//variables
int errorexists = 0;
String fname = "";
String lname = "";
String currentUserid = session.getAttribute("userid").toString();
String friendid = "";
String email = request.getParameter("email");
Class.forName("com.mysql.jdbc.Driver");
    
Connection connection = DriverManager.getConnection	("jdbc:mysql://localhost:3306/chat","root","1qaz!QAZ");
Statement statement = connection.createStatement() ;

	// check that user specified e-mail
	if(email.equals("")){
		errorexists = 1;
		out.println ("Please, add E-Mail <BR>");	
	}

	//check e-mail pattern
	pattern = Pattern.compile(EMAIL_PATTERN);
	matcher = pattern.matcher(email);
	if (!matcher.matches()) {
		out.println ("Please, verify e-mail format <BR> Go back to <a href='GUImainPage.jsp'>Main Page</a><BR>");
		errorexists = 1;
	}
   	
	//get id of the new friend
	ResultSet resultset2 = statement.executeQuery("select id, email, first_name, last_name from users where email = '"+email+"';");
		if (!resultset2.next()){
    		out.println ("No user with this e-mail registered. Go back to <a href='GUImainPage.jsp'>Main Page</a>");
    		errorexists = 1;
		}
		else
		{
			resultset2.first();
			friendid = resultset2.getString("id");
			fname = resultset2.getString("first_name");
			lname = resultset2.getString("last_name");
			}
	
	//verify that friend is not previously added
	if (errorexists == 0){
		ResultSet resultset = statement.executeQuery("SELECT ul.id FROM chat.users_link ul inner join chat.users on ul.related_user = users.id where (ul.id_user ='" + currentUserid + "' AND ul.related_user='" + friendid + "')");// or (ul.id_user ='" + friendid + "' AND ul.related_user='" + currentUserid + "');");
		if (resultset.first()){
			out.println("Account with the same e-mail is already your friend or your friendship request is still pending. <BR> Friend name: "+ fname +" "+ lname +"<BR>");
			out.println("Go back to <a href='GUImainPage.jsp'>Main Page</a>");
			errorexists = 1;
		}
	}
	
	//approve previously requested friendship
	if (errorexists == 0){
		ResultSet resultset = statement.executeQuery("SELECT ul.id FROM chat.users_link ul inner join chat.users on ul.related_user = users.id where (ul.id_user ='" + friendid + "' AND ul.related_user='" + currentUserid + "');");
		if (resultset.first()){
			String usersLinkkId = resultset.getString("id");
			statement.executeUpdate("update users_link set link_type = 2 where id='" + usersLinkkId + "';");
			out.println("Friendship request from your friend sucesfully approved. You are ready to chat with "+ fname +" "+ lname +"<BR>");
			out.println("Go back to <a href='GUImainPage.jsp'>Main Page</a>");
			errorexists = 2;//friendship sucessfully approved 
		}
	}
	
	//Insert new friendship request
    if (errorexists == 0){
       	statement.executeUpdate("INSERT INTO users_link (id_user, related_user, link_type) VALUES ('" + currentUserid + "','"+ friendid +"','1');");
   		out.println("New friend request sent, but you can already send some messeges (your friend will see them righr after friendship approval).<BR> Go back to <a href='GUImainPage.jsp'>Main Page</a>");
   	}
%>