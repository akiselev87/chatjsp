<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.util.regex.Matcher"%>
<%@ page import="java.util.regex.Pattern"%>
<%
//Features:
//- check that all fields are correct (not null, passwords matches, patters are correct)
//- check that user is not already added
//- add new user to the system

	//matchers and patterns
	Pattern pattern;
	Matcher matcher;
	String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	String PWD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()_+]).{6,20})"; 
	
	//variables
	String fname = request.getParameter("fname");
    String lname = request.getParameter("lname");
    String email = request.getParameter("email");
    String pwd = request.getParameter("pass");
    String pwdConfirm = request.getParameter("passConfirm");
    Class.forName("com.mysql.jdbc.Driver");
    int statementStatus = 0;
    int errorexists = 0;
	
    // Create mysql db connection
    Connection connection = DriverManager.getConnection	("jdbc:mysql://localhost:3306/chat","root","1qaz!QAZ");
	Statement statement = connection.createStatement() ;
	
	//Check that fields are not null
	if(fname.equals("")){
		errorexists = 1;
		out.println ("Please, add First Name <BR>");
	}
	
	if(lname.equals("")){
		errorexists = 1;
		out.println ("Please, add Last Name <BR>");
	}
	
	if(email.equals("")){
		errorexists = 1;
		out.println ("Please, add E-Mail <BR>");
	}
	else 
	//check e-mail pattern
	{
		pattern = Pattern.compile(EMAIL_PATTERN);
    	matcher = pattern.matcher(email);
    	if (!matcher.matches()) {
    		out.println ("Please, verify e-mail format <BR>");
    		errorexists = 1;
    	}
	}	
	
	//check that passwords exists 
	if(pwd.equals("") && pwdConfirm.equals("")){
		errorexists = 3;
		out.println ("Please, add Password and Confirmation Password <BR>");
	}
	else 
	//check password pattern
	{	pattern = Pattern.compile(PWD_PATTERN);
		matcher = pattern.matcher(pwd);
		if (!matcher.matches()) {
			out.println ("Password requirements: password between 6 to 20 characters which contain at least one numeric digit, onwe upper case lettwr, one lower case letter and a special character  <BR>");
			errorexists = 1;
		}
	}
	
	//confirm that both passwords equals
	if ((errorexists != 3) && !pwdConfirm.equals(pwd)) {
		errorexists = 1;
		out.println ("Confirm password is not the same as password <BR>");
    }
	
	//verify that e-mail is unique
	ResultSet resultset = statement.executeQuery("Select email From users");
	while (resultset.next()){
		String str = resultset.getString("email");
		if (str.equals(email)){
			out.println("Account with the same e-mail is already exists <BR>");
	        errorexists = 1;
			break;
		}
	}
	
	//Create a new user
  	if (errorexists == 0){
		try{
    	statementStatus = statement.executeUpdate("insert into users(first_name,last_name,email,user_password) values ('" + fname + "','" + lname + "','" + email + "','" + pwd + "')");
    	}
    	catch(SQLException e){
   		 	out.println (" Error: " + e);
    	}
  	}
  //Debug message  
  //  out.println("<BR> Results: ");
  //  ResultSet resultset = statement.executeQuery("Select * From users"); 
  //  while(resultset.next()){
  //  	out.println("<BR>");
  //  	out.println(resultset.getString("first_name"));
  //  }	
    
  // Navigation to next page
    if (statementStatus > 0) {
         out.print("Registration Successfully completed<BR>"+"<a href='GUIindex.jsp'>Go to Login</a>");
    	} else {
    		out.print("<BR>Registration failed. Please try again!<BR>"+"<a href='GUInewUser.jsp'>Go to Login</a>");
        }
%>


